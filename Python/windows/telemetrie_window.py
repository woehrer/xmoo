import logging
from tkinter.constants import DISABLED, END
import tkinter
import threading

def window(fenster):
    Label1 = tkinter.Label(fenster, text="X Statisch")
    Label2 = tkinter.Label(fenster, text="Y Statisch")
    Label3 = tkinter.Label(fenster, text="Z Statisch")
    Label4 = tkinter.Label(fenster, text="X Beschleunigung")
    Label5 = tkinter.Label(fenster, text="Y Beschleunigung")
    Label6 = tkinter.Label(fenster, text="Z Beschleunigung")

    statusbar = tkinter.Label(fenster, text="", bd=1, relief=tkinter.SUNKEN, anchor=tkinter.W)

    def tick():
        Label1.after(5000,tick)

    tick()

    Label1.grid(row=1, column=0)
    Label2.grid(row=2, column=0)
    Label3.grid(row=3, column=0)
    Label4.grid(row=4, column=0)
    Label5.grid(row=5, column=0)
    Label6.grid(row=6, column=0)

    statusbar.grid(row=8, column=0, columnspan=2, sticky='we')
