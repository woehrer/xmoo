import request

## Request Telemetrie
def request_telemetrie():
    url = "http://xmoo-master.local:5000/api/telemetrie"
    response = request.get(url)
    return response.json()